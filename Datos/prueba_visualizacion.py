import csv
import numpy as np 
from collections import Counter
from matplotlib import pyplot as plt

plt.style.use("fivethirtyeight")

with open('bdtf3840.csv') as csv_file:
	csv_reader = csv.DictReader(csv_file)

	degree_counter = Counter()

	for row in csv_reader:
		degree_counter.update(row['advisor'].split(';'))

advisor = []
popularity = []
for item in degree_counter.most_common(15):
	advisor.append(item[0])
	popularity.append(item[1])

#degree()
#popularity()

plt.barh(advisor, popularity)

plt.title("Grados más populares")
plt.ylabel("grados")
plt.xlabel("Popularidad")
#plt.legend(['línea'])
plt.grid(True)
plt.tight_layout()
#plt.savefig('prueb-plot-bar')
plt.show()

#print(languague_counter.most_common(15))